module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      colors: {
        'primary': '#151721',
        'secondary': '#0e0e14',
        'terciary': '#2B3044',
        'body': '#161623',
        'border': '#32384f',
      },
      backgroundImage: theme => ({
        'hero-saytv': "url('/src/assets/images/bg.png')",
      })
    },
  },
  variants: {},
  plugins: [
    require('@tailwindcss/ui'),
  ],
}
